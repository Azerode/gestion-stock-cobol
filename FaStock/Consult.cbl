       program-id. Consult as "FaStock.Consult".

       data division.
       working-storage section.
      ***************************************************************************************************************
      * Structure de donn�es                                                                                        *
      ***************************************************************************************************************
      * D�finition des donn�es pour un fournisseur
       01 fournisseur.
           05 nom-fournisseur sql type char-varying(50).
           05 adresse-fournisseur sql type char-varying(130).
      * D�finition des donn�es pour un article
       01 article.
           05 id-article pic x(36).
           05 nom-article sql type char-varying(50).
           05 reference-article sql type char-varying(50).
           05 description pic x(50).
           05 prix pic 9(10)V99.
           05 quantite pic 9(10)v99.
           05 stock-minimum pic 9(10)v99.
           05 stock-maximum pic 9(10)v99.
      * Tableau pour stocker une page.    
       01 tableau-article.
           05 taille pic 99 comp value 0.
           05 case occurs 10.
               10 case-id pic x(36).
               10 case-nom sql type char-varying(50).
               10 case-reference sql type char-varying(50).
               10 case-prix pic 9(10)V99.
               10 case-quantite pic 9(10)v99.
               10 case-stock-minimum pic 9(10)v99.
               10 case-stock-maximum pic 9(10)v99.
      ***************************************************************************************************************
      * Variables                                                                                                   *
      ***************************************************************************************************************
       77 response pic 9.
       77 fin-consultation pic 9 comp.
       77 fin-detail pic 9 comp.
       77 fin-exec pic 9 comp.
       77 not-found pic 9 comp.
       77 erreur pic x(80).
       77 ligne-affichage pic 99.
       77 comptage pic 99 comp.
       77 option-consultation pic x.
       77 option-detail pic 99.
       77 CouleurFondEcran pic 99 value 0.
       77 CouleurCaractere pic 99 value 2.
       77 CouleurDate pic 99 value 6.
       77 CouleurOption pic 99 value 15.
      * Variable pour la gestion des pages
       77 debut-page pic 9(18).
       77 fin-page pic 9(18).
       linkage section.
           exec sql
               include sqlca
           end-exec.
       01 DateSysteme.
           10 Annee pic 99.
           10 Mois pic 99.
           10 Jour pic 99.
       screen section.
      * Ligne � afficher lors de la consultation des articles pour un fournisseur
       01 ligne-consultation-article.
           05 line ligne-affichage col 1 pic 99 from comptage.
           05 line ligne-affichage col 4 pic x(20) from case-nom(comptage).
           05 line ligne-affichage col 25 pic x(20) from case-reference(comptage).
           05 line ligne-affichage col 40 pic z(9)9V,99 from case-quantite(comptage).
           05 line ligne-affichage col 53 pic z(9)9V,99 from case-prix(comptage).
           05 line ligne-affichage col 68 pic z(9)9V,99 from case-stock-minimum(comptage).
       01 ligne-detail.
           05 line ligne-affichage col 1 pic 99 from comptage.
           05 line ligne-affichage col 4 pic x(20) from nom-article.
           05 line ligne-affichage col 25 pic x(20) from reference-article.
           05 line ligne-affichage col 40 pic z(10)9v,99 from quantite.
           05 line ligne-affichage col 53 pic z(10)9v,99 from prix.
      * Ecran de la fonction Consultation des stocks
       01 Consultation-des-stocks  background-color is CouleurFondEcran foreground-color is CouleurCaractere.
           10 line 1 col 1 blank screen.
           10 line 24 col 02 from Jour of DateSysteme foreground-color is CouleurDate.
           10 line 24 col 04 value "/" foreground-color is CouleurDate.
           10 line 24 col 05 from Mois of DateSysteme foreground-color is CouleurDate.
           10 line 24 col 07 value "/" foreground-color is CouleurDate.
           10 line 24 col 08 from Annee of DateSysteme foreground-color is CouleurDate.
           10 line 2 col 30 value "Consultation des Stocks".
           10 line 5 col  5 value "Nom du fournisseur     :".
           10 line 5 col 30 from nom-fournisseur foreground-color is CouleurOption.
           10 line 6 col  5 value "Adresse du founisseur  :".
           10 line 6 col 30 from adresse-fournisseur foreground-color is CouleurOption.
           10 line 8 col  1 value "   Article               Reference       Quantite    Prix Unitaire  Quantite min" background-color is CouleurCaractere foreground-color is CouleurFondEcran.
      *    10 line 8 col 61 from euro  background-color is CouleurCaractere foreground-color is CouleurFondEcran.
           10 line 20 col  1 pic x(80) value all "_".
      * Menu pour choisir pqge suivante, pr�cedente, detail etc... 
       01 Details.
           10 line 23 col  1 pic x(80) value all space.
           10 line 22 col  1 pic x(80) value all space.
           10 line 24 col  1 pic x(80) value all space.
           10 line 22 col 3 value "Page [S]uivante, Page [P]recedente, [R]etour".
           10 line 23 col 60 value "Option : ".
           10 line 23 col 69 from option-consultation foreground-color is CouleurOption.
       01 ecran-erreur.
           05 line 1 col 1 from erreur  foreground-color is CouleurCaractere.
      * Code du programme
       procedure division using sqlca DateSysteme.

       consultation.
           perform initialisation-consultation.
           perform traitement-consultation until fin-exec = 1.
           perform end-consultation.

       initialisation-consultation.
           move 0 to fin-exec.
       traitement-consultation.
           initialize fournisseur.
           display Consultation-des-stocks.
           display ecran-erreur.
           accept nom-fournisseur line 5 col 30.
           if nom-fournisseur not equal space then
               perform chargement-article
           else
               move 1 to fin-exec
           end-if.
       end-consultation.
           perform fin-programme.
      * Paragraphe qui permet la fermeture du programme.
       fin-programme.
           goback.
      * Affichage du menu de consultation
       affichage-menu-consultation.
           display Consultation-des-stocks.
           display Details.
      **************************************************************************************************************
      * Module pour charger les articles d'un fournisseur (dont le nom est unique)                                 *
      **************************************************************************************************************
       chargement-article.
           perform initilisation-consultation-article.
           perform traitement-consultation-article until fin-consultation = 1 or not-found = 1.
           perform fin-consultation-article.
      * Initialisation de la consultation
       initilisation-consultation-article.
           move 0 to fin-consultation.
           move space to option-consultation.
           move 0 to comptage.
           move 0 to debut-page.
           move 10 to fin-page.
           move space to erreur.
           perform module-chargement-article.
      * Mise en place de la consultation
       traitement-consultation-article.
           perform affichage-menu-consultation.
           perform module-affichage-consultation.
           accept option-consultation line 23 col 69.
           evaluate true
               when option-consultation = "S" or option-consultation = "s"
                   perform lecture-suivante
               when option-consultation = "P" or option-consultation = "p"
                   perform lecture-precedente
               when option-consultation = "D" or option-consultation = "d"
                   perform module-detail-article
               when option-consultation = "R" or option-consultation = "r"
                   move 1 to fin-consultation
                   move space to erreur
           end-evaluate.
      * Fin de la consultation
       fin-consultation-article.
           continue.
      * Lecture de la page suivante
       lecture-suivante.
           if ligne-affichage = 19 then 
               add 10 to debut-page
               add 10 to fin-page
               perform module-chargement-article
           end-if.
      * Lecture de la page pr�c�dente
       lecture-precedente.
           if debut-page > 0 then
               subtract 10 from debut-page giving debut-page
               subtract 10 from fin-page giving fin-page
               perform module-chargement-article
           end-if.
      *************************************************************************************************************
      * Module chargement du tableau contenant les articles                                                       *
      *************************************************************************************************************
       module-chargement-article.
           perform initialisation-chargement.
           perform traitement-chargement varying comptage from 1 by 1 until comptage > 10 or (sqlcode = 100 or sqlcode= 101).
           perform fin-chargement.
      * Initialisation du menu de chargement
       initialisation-chargement.
           move 0 to taille.
           move 0 to not-found.
      * Mise en place du curseur
           exec sql
               declare cr-article cursor for
               select idarticle, nomarticle, reference, avg(prix) as prix, sum(valeurstock), nomfournisseur, adresse, stockmini
               from (select *, ROW_NUMBER() OVER (ORDER BY idarticle) as ligne from vue_article where nomfournisseur = :nom-fournisseur) as vue_article
               where ligne between :debut-page and :fin-page
               group by reference, nomarticle, nomfournisseur, adresse, idarticle, stockmini
               order by idarticle
           end-exec.
      * Ouverture du curseur
           exec sql
             open cr-article 
           end-exec.
      * Test du retour de l'ouverture du curseur
           if sqlcode not equal 0 then
               move "erreur chargement" to erreur
           end-if.
      * Mise en placede chaque article dans le tableau
       traitement-chargement.
           exec sql
               fetch cr-article into 
                   :id-article, :nom-article, :reference-article,
                   :prix, :quantite, :nom-fournisseur,
                   :adresse-fournisseur, :stock-minimum
           end-exec.
           if sqlcode not equal 100 and sqlcode not equal 101 then
               move id-article to case-id(comptage)
               move nom-article to case-nom(comptage)
               move prix to case-prix(comptage)
               move quantite to case-quantite(comptage)
               move reference-article to case-reference(comptage)
               move stock-minimum to case-stock-minimum(comptage)
               add 1 to taille
           else
               move "Aucune donn�e trouv�e" to erreur
               if comptage = 1 then
                 move 1 to not-found
               end-if
           end-if.
      *Fin du module de chargement
       fin-chargement.
           exec sql
             close cr-article
           end-exec.
      *************************************************************************************************************
      * Module d'affichage des donn�es du tableau                                                                 *
      *************************************************************************************************************
       module-affichage-consultation.
           perform initialisation-affichage.
           perform traitement-affichage varying comptage from 1 by 1 until comptage > taille.
           perform fin-affichage.
      * Initialisation de l'affichage.
       initialisation-affichage.
           move 9 to ligne-affichage.
      * Parcours de toutes les lignes � afficher.
       traitement-affichage.
           display ligne-consultation-article.
           add 1 to ligne-affichage.
      * Fin de l'affichage
       fin-affichage.
           continue.
      ***************************************************************************************************************
      * Module pour consulter le d�tail des stocks                                                                  *
      ***************************************************************************************************************
       module-detail-article.
           perform detail-initialisation.
           perform detail-traitement until fin-detail = 1.
           perform detail-fin.
      * Initialisation du module detail.
       detail-initialisation.
           move 0 to fin-detail.
           move 9 to ligne-affichage.
           move 0 to comptage.
           accept option-detail.
           if option-detail is numeric then 
               move case-reference(option-detail) to reference-article
           end-if.
           exec sql 
             declare cr-detail-article cursor for
             select idarticle,nomarticle, reference, prix, valeurstock, stockmini, stockmaxi
             from vue_article
             where reference = :reference-article
           end-exec.
           exec sql
             open cr-detail-article
           end-exec.
           if sqlcode not equal 0 then
               move "erreur ouverture curseur detail" to erreur
           end-if.
           perform affichage-menu-consultation.
      * Affichage des lignes des stocks
       detail-traitement.
           exec sql 
             fetch cr-detail-article into 
               :id-article, :nom-article, :reference-article,
               :prix, :quantite, :stock-minimum,
               :stock-maximum
           end-exec.
           if sqlcode equal 100 or sqlcode equal 101 then
               move 1 to fin-detail
           else
               add 1 to comptage
               display ligne-detail
               add 1 to ligne-affichage
           end-if.
      * Fin du module de detail des articles
       detail-fin.
           exec sql
             close cr-detail-article
           end-exec.
           accept response.
       end program Consult.