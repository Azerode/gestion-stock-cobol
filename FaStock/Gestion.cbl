       program-id. Gestion as "FaStock.Gestion".
       environment division.
       input-output section.
      * Fichier pour ajouter les nouvelles quantit�s dans les stock
       file-control.
       select fichier-commande assign to dynamic file-name
       organization line sequential
       access sequential
       file status is file-status.
       select fichier-rapport assign to dynamic rapport-file-name
       organization line sequential
       access sequential.
       data division.
       file section.
           fd fichier-commande.
           01 ligne-fichier-commande pic x(88).
           fd fichier-rapport.
           01 ligne-fichier-rapport pic x(80).
       working-storage section.
      ***************************************************************************************************************
      * Structure de donn�es                                                                                        *
      ***************************************************************************************************************
      * Date pour le syst�me
       01 date-systeme.
           10 Annee pic 99.
           10 Mois pic 99.
           10 Jour pic 99.
       01 file-status.
           05 file-status1 pic x.
           05 file-status2 pic x.
      * D�finition des donn�es pour un fournisseur
       01 nom-fournisseur.
           05 filler pic x(54).
           05 nom sql type char-varying(50).
      * Donn�es de l'adresse de l'exp�diteur
       01 adresse-1.
           05 filler pic x(54).
           05 adresse-fournisseur-1 sql type char-varying(50).
       01 adresse-2.
           05 filler pic x(54).
           05 adresse-fournisseur-2 sql type char-varying(50).
      * Date pour le nom du fichier
       01 date-fichier.
           05 d pic xx.
           05 filler value "-".
           05 m pic xx.
           05 filler value "-".
           05 y pic xx.
      * Date pr�sent dans le fichier
       01 ligne-date-fichier.
           05 filler pic x(54).
           05 dd pic xx.
           05 filler pic x value "/".
           05 mm pic xx.
           05 filler pic x value "/".
           05 yy pic xx.
      * Ligne contenant les informations des articles
       01 ligne-article.
           05 filler pic x(9).
           05 nom-article pic x(22).
           05 filler pic x.
           05 reference-article pic x(21).
           05 filler pic x.
           05 quantite pic x(14).
           05 number-quantite redefines quantite pic 9(10)V99.
           05 filler pic x.
           05 prix pic x(13).
      * Ligne separateur pour le rapport
       01 ligne-separateur.
           05 filler pic x value "+".
           05 filler pic x(78) value all "=".
           05 filler pic x value all "+".
      * Ligne separateur pour le rapport niveau 2
       01 ligne-separateur-2 pic x(80) value all "-".
      * Ligne de fin de tableau
       01 ligne-tableau.
           05 filler pic x.
           05 filler pic x value "+".
           05 filler pic x(22) value all "-".
           05 filler pic x value "+".
           05 filler pic x(21) value all "-".
           05 filler pic x value "+".
           05 filler pic x(14) value all "-".
           05 filler pic x value "+".
           05 filler pic x(13) value all "-".
           05 filler pic x value "+".
           05 filler pic x(8).
      * Ligne pour les case de l'entete du tableau
       01 ligne-entete-tableau.
           05 filler pic x.
           05 filler value "+".
           05 filler pic x(22) value all "=".
           05 filler value "+".
           05 filler pic x(21) value all "=".
           05 filler value "+".
           05 filler pic x(14) value all "=".
           05 filler value "+".
           05 filler pic x(13) value all "=".
           05 filler value "+".
      * Status erreur pour le fichier
       01 status-erreur.
           05 filler pic x(46).
           05 filler value "|".
           05 filler value "Modification".
           05 filler pic x(2).
           05 filler value ": ".
           05 filler value "Erreur".
           05 filler pic x(6).
           05 filler value "|".
      * Titre de la commande
        01 titre.
           05 filler pic x(36).
           05 filler pic x(12) value "Commande n� ".
           05 numero pic x(36).
           05 filler pic x(37).
       01 bon-commande.
           05 filler value "Bon de commande num�ro : ".
           05 bon pic x(36).
      * Status ok pour le fichier
       01 status-ok.
           05 filler pic x(46).
           05 filler value "|".
           05 filler value "Modification".
           05 filler pic x(2).
           05 filler value ": ".
           05 filler value "OK".
           05 filler pic x(10).
           05 filler value "|".
           exec sql
               include sqlca
           end-exec.
      ***************************************************************************************************************
      * Variables                                                                                                   *
      ***************************************************************************************************************
       77 connexion-database pic x(255) value "Trusted_Connection=yes;Database=FaStock;server=localhost\SQLEXPRESS;factory=System.Data.SqlClient;".
      * Variables pour les diff�rents fichiers
       77 file-name pic x(255).
       77 path pic x(255) value "C:\Users\Azero\Documents\Import\".
       77 rapport-file-name pic x(255).
      * Variables pour la fin.
       77 fin-importation pic 9 comp.
       77 fin-lecture pic 9 comp.
      * Variable incrementable pour le nom du fichier
       77 comptage pic 9(4).
       77 numero-ligne pic 9(4) comp.
      * Variable pour la base de donn�es.
       77 adresse pic x(130).
      * Variable existence
       77 nombre-article pic 99 comp.
       77 article-existe-commande pic s9(4) comp.
      * Variable d'erreur.
       77 erreur pic 9 comp.
       77 ligne-valided pic 99 comp.
       procedure division.
      ***************************************************************************************************************
      * MODULE IMPORTATION                                                                                          *
      ***************************************************************************************************************
      * Importation des stocks
       importation.
           perform importation-initialisation.
           perform importation-traitement until fin-importation = 1.
           perform importation-fin.
      * Initialisation de l'importation
       importation-initialisation.
      * Connexion � la base de donn�e
           exec sql
             connect using :connexion-database
           end-exec.
           if sqlcode not equal 0 then
               display "error connexion database"
           end-if.
           accept date-systeme from date.
           move 0 to fin-importation.
           move 1 to comptage.
           move jour to d.
           move mois to m.
           move annee to y.
           string 
             path delimited by "  "
             "Rapport-commande"
             "-"
             date-fichier
             ".txt"
             into 
             rapport-file-name
           end-string.
           open extend fichier-rapport.
           perform ecrire-entete.
      * Traitement de l'importation
       importation-traitement.
      * Cr�ation du nom du fichier
           string 
             path delimited by  "  "
             date-fichier "_"
             comptage
             ".txt"
             into 
             file-name
           end-string.
           perform lecture-fichier.
           add 1 to comptage.
      * Fin de l'importation
       importation-fin.
           perform ecrire-pied-page.
           close fichier-rapport.
           goback.
      ***************************************************************************************************************
      * MODULE DE LECTURE D'UN FICHIER DE COMMANDE                                                                  *
      ***************************************************************************************************************
      * Lecture du fichier de commande
       lecture-fichier.
           perform lecture-fichier-initialisation.
           perform lecture-fichier-traitement until fin-lecture = 1.
           perform lecture-fichier-fin.
      * Initialisation de la lecture du fichier
       lecture-fichier-initialisation.
           move 0 to numero-ligne.
           move 0 to ligne-valided.
           move 0 to erreur.
           exec sql
             begin tran nouveau-product
           end-exec
           open input fichier-commande.
           evaluate file-status
               when '00' move 0 to fin-lecture
               when '35'
                   move 1 to fin-lecture
                   move 1 to fin-importation
               when other move 1 to fin-lecture
           end-evaluate.
      * Traitement de la lecture du fichier
       lecture-fichier-traitement.
           add 1 to numero-ligne.
           read fichier-commande
               at end move 1 to fin-lecture
               not at end perform verification
           end-read.
      * Fin de la lecture du fichier
       lecture-fichier-fin.
           if ligne-valided = nombre-article  and ligne-valided > 0 then 
               exec sql 
                 update commande 
                 set 
                       status = 'Valided.'
                 where idcommande = :numero
               end-exec
           end-if.
           if erreur = 1 then
               if file-status <> '35' then 
                   write ligne-fichier-rapport from " Erreur : fichier non pris en compte"
               end-if
               exec sql
                 rollback
               end-exec
           else 
               if file-status <> '35' and ligne-valided > 0 then
                   write ligne-fichier-rapport from "Fichier pris en compte (modification valide)"
               end-if
               if file-status <> '35' and ligne-valided = 0 then
                    write ligne-fichier-rapport from "Fichier d�j� valid� (aucunes modifications faites)"
               end-if
               exec sql
                 commit
               end-exec
           end-if
           if file-status = 0 then 
               write ligne-fichier-rapport from ligne-separateur-2
           end-if
           close fichier-commande.
      * V�rification
       verification.
           evaluate true
               when ligne-fichier-commande(9:1) = "|" and numero-ligne > 19 perform article-liste
               when ligne-fichier-commande(9:1) = "|" write ligne-fichier-rapport from ligne-fichier-commande(8:80)
               when ligne-fichier-commande(9:2) = "+=" write ligne-fichier-rapport from ligne-entete-tableau
               when ligne-fichier-commande not equal space perform verification-ligne
           end-evaluate.
      * V�rification d'une ligne non vide
       verification-ligne.
           evaluate numero-ligne
               when 8
                   move ligne-fichier-commande to nom-fournisseur
                   write ligne-fichier-rapport from ligne-separateur-2
                   write ligne-fichier-rapport from nom
               when 9 move ligne-fichier-commande to adresse-1
               when 10 move ligne-fichier-commande to adresse-2
               when 13
                   move ligne-fichier-commande to ligne-date-fichier
                   write ligne-fichier-rapport from ligne-date-fichier
               when 16 
                   move ligne-fichier-commande to titre
                   move numero to bon
                   write ligne-fichier-rapport from space
                   write ligne-fichier-rapport from space
                   write ligne-fichier-rapport from bon-commande
                   write ligne-fichier-rapport from space
                   exec sql
                     select count(*) into :nombre-article from articlecommande 
                     where idcommande = :numero and status = 'Pending.'
                   end-exec
           end-evaluate.
      * V�rification d'une article-liste
       article-liste.
           move ligne-fichier-commande to ligne-article.
           perform creation-adresse-fournisseur.
           perform mise-a-jour.
      * Cr�ation de l'adresse du fournisseur
       creation-adresse-fournisseur.
           string 
             adresse-fournisseur-1 delimited by "  "
             space
             adresse-fournisseur-2 delimited by "  "
             into 
               adresse
             end-string.
      * Mise � jour de la base de donn�es
       mise-a-jour.
           if nombre-article >= 1 then
               move "." to quantite(12:1)
               move function numval(quantite) to number-quantite
               if number-quantite > 0 then 
                   exec sql
                     select count(*) into :article-existe-commande from articlecommande
                     where referencearticle = :reference-article  and idcommande = :numero
                     and status = 'Pending.'
                   end-exec
                   if article-existe-commande  = 1 then
                       exec sql
                           update stock 
                           set valeurstock = (valeurstock + :number-quantite)
                           where idarticle =
                           (select idarticle from article where reference = rtrim(:reference-article)
                           and idfournisseur = (select idfournisseur from fournisseur where 
                             nomfournisseur = rtrim(:nom))
                           )
                       end-exec
                       write ligne-fichier-rapport from ligne-fichier-commande(8:80)
                       write ligne-fichier-rapport from ligne-tableau
                       if sqlcode <> 0 then
                          write ligne-fichier-rapport from status-erreur
                          write ligne-fichier-rapport from space
                          move 1 to erreur
                       else
                           write ligne-fichier-rapport from status-ok
                           write ligne-fichier-rapport from space
                           exec sql
                               update articlecommande
                               set 
                                 status = 'Valided.'
                               where idcommande = :numero and referencearticle = :reference-article
                           end-exec
                           add 1 to ligne-valided
                       end-if
                   end-if
               end-if
           end-if.
      ***************************************************************************************************************
      * MODULE ECRITURE RAPPORT                                                                                     *
      ***************************************************************************************************************
      * Disposition de l'ent�te
       ecrire-entete.
           write ligne-fichier-rapport from ligne-separateur.
           move space to ligne-fichier-rapport.
           string 
             "Date execution :" space date-fichier
             into ligne-fichier-rapport
           end-string.
           write ligne-fichier-rapport.
           write ligne-fichier-rapport from space.
           write ligne-fichier-rapport from space.
      * Disposition du pied de page
       ecrire-pied-page.
           write ligne-fichier-rapport from space.
           write ligne-fichier-rapport from space.
           write ligne-fichier-rapport from ligne-separateur.

       end program Gestion.