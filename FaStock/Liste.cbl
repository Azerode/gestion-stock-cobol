       program-id. Liste as "FaStock.Liste".

       data division.
       working-storage section.

       01 fournisseur.
           05 id-fournisseur       pic x(36).
           05 nom-fournisseur pic x(50).
           05 id-adresse          pic x(36).
           05 adresse-fournisseur.
               10 No-Rue            pic x(3).
               10 nom-rue           pic x(20).
               10 code-postal       pic 9(5).
               10 ville            pic x(15).
               10 pays             pic x(15).


       01 article.
           05 id-article pic x(36).
           05 nom-article pic x(50).
           05 reference-article pic x(50).     
           05 description pic x(50).
           05 prix pic 9(10)V,99.
           05 quantite pic 9(10)v99.
           05 stock-minimum pic 9(10)9v99.
           05 stock-maximum pic 9(10)9v99.



       77 CouleurFondEcran         pic 99 value 0.
       77 CouleurCaractere         pic 99 value 2.
       77 CouleurDate              pic 99 value 6.
       77 CouleurOption            pic 99 value 15.
       77 Liste-option             pic x.
       77 LigneListe              pic 99.
       77 ListeFin                 pic x.
       77 NoLigneEfface            pic 99.
       linkage section.
           exec sql
               include sqlca
           end-exec.
       01 DateSysteme.
           10 Annee pic 99.
           10 Mois pic 99.
           10 Jour pic 99.

       screen section.
       01 Ecran-liste background-color is CouleurFondEcran foreground-color is CouleurCaractere.
           10 line  1 col  1 Blank Screen.
           10 line  2 col 32 value "Liste des stocks ".
           10 LINE  5 col  1 pic x(80) value all space background-color is CouleurCaractere foreground-color is CouleurFondEcran.
           10 LINE  5 col  2 value "Nom" background-color is CouleurCaractere foreground-color is CouleurFondEcran.
           10 LINE  5 col 20 value "Reference"  background-color is CouleurCaractere foreground-color is CouleurFondEcran.
           10 line  5 col 30 value "Fournisseur" background-color is CouleurCaractere foreground-color is CouleurFondEcran.
           10 line  5 col 47 value "Quantite" background-color is CouleurCaractere foreground-color is CouleurFondEcran.
           10 line  5 col 58 value "Stock Mini" background-color is CouleurCaractere foreground-color is CouleurFondEcran.
           10 line  5 col 70 value "Stock Maxi" background-color is CouleurCaractere foreground-color is CouleurFondEcran.
           10 line 24 col 2 from Jour of DateSysteme foreground-color is CouleurDate.
           10 line 24 col 4 value "/" foreground-color is CouleurDate.
           10 line 24 col 5 from Mois of DateSysteme foreground-color is CouleurDate.
           10 line 24 col 7 value "/" foreground-color is CouleurDate.
           10 line 24 col 8 from Annee of DateSysteme foreground-color is CouleurDate.
       01 Liste-Question background-color is CouleurFondEcran foreground-color is CouleurCaractere.
           10 line  1 col  2 value " Page [S]uivante - Retour au [M]enu  : " background-color is CouleurCaractere foreground-color is CouleurFondEcran.
           10 line  1 col 41 from Liste-Option background-color is CouleurCaractere foreground-color is CouleurFondEcran.

       01 Liste-Ligne background-color is CouleurFondEcran foreground-color is CouleurCaractere.
           10 line LigneListe col  1 value all space.
           10 line LigneListe col  2 from nom-Article pic x(17).
           10 line LigneListe col 20 from reference-article pic x(10).
           10 line LigneListe col 30 from nom-fournisseur pic x(15).
           10 line LigneListe col 45 from quantite pic z(6)9v,99.
           10 line LigneListe col 56 from stock-minimum pic z(6)9v,99.
           10 line LigneListe col 67 from stock-maximum pic z(6)9v,99.
       01 Ligne-Efface background-color is CouleurFondEcran foreground-color is CouleurCaractere.
           10 line LigneListe col 1 pic x(80) value all space.

       01 Ecran-fin background-color is CouleurFondEcran foreground-color is CouleurCaractere.
           10 line LigneListe col 1 pic x(80) value all space.
       procedure division using sqlca DateSysteme.
       ListeCompte.
           perform Liste-Init.
           perform Liste-Trt Until ListeFin = 1.
           perform Liste-Fin.


       Liste-Init.
       
      * Initialisation de la boucle de lecture


           move 0 to ListeFin.
       
      * Affichage de l'�cran
           display Ecran-liste.
           
      * Cr�ation du curseur
           exec sql
               Declare Liste-Cursor cursor for
                   select Nomarticle, reference, valeurstock, stockmini, stockmaxi, nomfournisseur from Vue_article
           end-exec.
           
      * Ouverture du curseur
           exec sql
               open Liste-Cursor
           end-exec.
           
      * Initialisation de la pagination
           move 5 to ligneliste.
           
      * traitement d'une ligne de la liste des banques
       Liste-Trt.
       
      * On lit une banque � partir du curseur
           exec sql
               fetch Liste-Cursor into :nom-article, :reference-article, :quantite, :stock-minimum, :stock-maximum, :nom-fournisseur
           end-exec.
           if sqlcode = 100 or SQLCODE = 101 then
               move 1 to ListeFin
               add 1 to LigneListe
               display " Fin de la liste des comptes tapez entree " line 1 col 1 with no advancing
               perform effacer varying LigneListe from LigneListe by 1 until LigneListe = 23
               accept Liste-option

           else
               perform Liste-Affichage
           end-if.
           
       Liste-Affichage.
       
      * On passe � la ligne suivante
           add 1 to LigneListe.
           
      * On affiche la ligne.
           display liste-ligne.
           
      * Si on est sur la derni�re ligne, on demande si on passe � la page suivante
           if LigneListe = 23 then
               move "S" to Liste-option
               display Liste-Question
               accept Liste-option line 1 col 41
               move 1 to NoLigneEfface
               display Ligne-Efface
               move 5 to LigneListe
               if Liste-option = "M" or Liste-option = "m" then move 1 to ListeFin
           end-if.
      * Ecran blanc
       effacer.
           display Ecran-fin.

      * Fin de la liste des banques
       Liste-fin.
           exec sql
               close Liste-Cursor
           end-exec.



       
           
       end program Liste.