       program-id. Main as "FaStock.Main".
       data division.
       working-storage section.
      *************************************************************************
      * Structure de donn�es                                                  *
      *************************************************************************
       01 DateSysteme.
           10 Annee pic 99.
           10 Mois pic 99.
           10 Jour pic 99.
      *************************************************************************
      * Variable                                                              *
      *************************************************************************
      *77 connexion-database pic x(255) value "Server=localhost;Database=Fastock;dsn=Fastock;".
       77 connexion-database pic x(255) value "Trusted_Connection=yes;Database=FaStock;server=localhost\SQLEXPRESS;factory=System.Data.SqlClient;".
       77 CouleurFondEcran pic 99 value 0.
       77 CouleurCaractere pic 99 value 2.
       77 CouleurDate pic 99 value 6.
       77 CouleurOption pic 99 value 15.
       77 option pic x.
          exec sql
              include sqlca
          end-exec.
       screen section.
      ***Ecran principale du menu***
       01 menu-Fastock  background-color is CouleurFondEcran foreground-color is CouleurCaractere.
           10 line  1 col  1 Blank Screen.
           10 line  3 col 36 value " FaStock ".
           10 line  5 col  2 value " Date systeme :" foreground-color is CouleurDate.
           10 line  5 col 18 from Jour of DateSysteme foreground-color is CouleurDate.
           10 line  5 col 20 value "/" foreground-color is CouleurDate.
           10 line  5 col 21 from Mois of DateSysteme foreground-color is CouleurDate.
           10 line  5 col 23 value "/" foreground-color is CouleurDate.
           10 line  5 col 24 from Annee of DateSysteme foreground-color is CouleurDate.
           10 line  5 col 69 value " Option :".
           10 line  5 col 79 pic 9 from Option foreground-color is CouleurOption.
           10 line  9 col 25 value "- 1 - Consultation des stocks       :".
           10 line 11 col 25 value "- 2 - Menu Modification             :".
           10 line 13 col 25 value "- 3 - Menu Ajout                    :".
           10 line 15 col 25 value "- 4 - Liste des stocks              :".
           10 line 17 col 25 value "- 5 - Generer Commande              :".
           10 line 19 col 25 value "- 6 - Importer Commande             :".
           10 line 23 col 25 value "- 0 - Quitter FaStock               :".
       procedure division.
           perform debut-database.
           perform Menu-init.
           perform Menu-trt until option = 0.
           perform Menu-fin.
      *Paragraphe permettant la connexion � la base de donn�e.
       debut-database.
      * Mise en place la connexion � la base de donn�es.
           exec sql
             connect using :connexion-database
           end-exec.
      * V�rification de la requete pour se connecter.
           if sqlcode not equal 0 then
               display "error connexion database"
               accept option
               perform Menu-fin
           end-if.
       Menu-init.
           move 9 to option.
           accept DateSysteme from Date.
       Menu-trt.
           move 0 to option.
           display menu-Fastock.
           accept option line 5 col 79.
           evaluate option
               when 0
                   continue
               when 1
                   call "Consult" using SQLCA DateSysteme
               when 2
                   call "Modification" using sqlca DateSysteme
               when 3 
                   call "Ajout" using SQLCA DateSysteme
               when 4
                   call "Liste" using SQLCA DateSysteme
               when 5
                   call "Commande"
               when 6 
                   call "Gestion"
           end-evaluate.
       Menu-fin.
           stop run.

       end program Main.