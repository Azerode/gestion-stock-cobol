       program-id. Modification as "FaStock.Modification".
       data division.
       working-storage section.
      *************************************************************************************************************
      * Structure de donn�e                                                                                       *
      *************************************************************************************************************
      * D�finition des donn�es pour un fournisseur
       01 fournisseur.
           05 id-fournisseur pic x(36).
           05 nom-fournisseur sql type char-varying(50).
           05 adresse.
               10 id-adresse pic x(36).
               10 no-rue pic 999.
               10 rue sql type char-varying(50).
               10 code-postal pic x(5).
               10 ville sql type char-varying(20).
               10 pays sql type char-varying(20).
      * D�finition des donn�es pour un article
       01 article.
           05 id-article pic x(36).
           05 id-stock pic x(36).
           05 nom-article sql type char-varying(50).
           05 description sql type char-varying(50).
           05 reference-article sql type char-varying(50).
           05 prix pic 9(10)V99.
           05 quantite pic 9(10)v99.
           05 stock-minimum pic 9(10)v99.
           05 stock-maximum pic 9(10)v99.
      *************************************************************************************************************
      * Variable                                                                                                  *
      *************************************************************************************************************
       77 CouleurFondEcran pic 99 value 0.
       77 CouleurCaractere pic 99 value 2.
       77 CouleurDate pic 99 value 6.
       77 CouleurOption pic 99 value 15.
       77 option-modification pic x.
       77 option-suppression pic x.
       77 option-validation pic x.
       77 modification-fin pic 9.
       77 fin-donnee pic 9.
       77 message-error pic x(80).
       77 always-exist pic 999.
       linkage section.
      * Donn�e pour g�rer les code d'erreur sql
           exec sql
               include sqlca
           end-exec.
      * Date du syst�me
       01 DateSysteme.
           10 Annee pic 99.
           10 Mois pic 99.
           10 Jour pic 99.
       screen section.
      * Encran de d�part pour la modification 
       01 modification-des-stocks background-color is CouleurFondEcran foreground-color is CouleurCaractere.
           10 line 1 col 1 blank screen.
           10 line 24 col 2 from Jour of DateSysteme foreground-color is CouleurDate.
           10 line 24 col 4 value "/" foreground-color is CouleurDate.
           10 line 24 col 5 from Mois of DateSysteme foreground-color is CouleurDate.
           10 line 24 col 7 value "/" foreground-color is CouleurDate.
           10 line 24 col 8 from Annee of DateSysteme foreground-color is CouleurDate.
           10 line 3 col 34 value "Modification".
           10 LINE 6 col 1 pic x(80) value all space background-color is CouleurCaractere foreground-color is CouleurFondEcran.
           10 line 6 col 2 value "Que souhaitez vous modifier? [F]ournisseur, [A]rticle, [R]etour :" background-color is CouleurCaractere foreground-color is CouleurFondEcran.
           10 line 6 col 68 from option-modification background-color is CouleurCaractere foreground-color is CouleurOption.
      * Ecran de d�part de la modification de fournisseur
       01 ecran-modification-fournisseur background-color is CouleurFondEcran foreground-color is CouleurCaractere.
           10 line 6 col 1 pic x(80) value all space background-color is CouleurCaractere .
           10 line 6 col 1 value " Quel fournisseur voulez-vous modifiez ?" background-color is CouleurCaractere  foreground-color is CouleurFondEcran.
           10 line 8 col 5 value "Nom du fournisseur      :".
           10 line 8 col 31 from nom-fournisseur foreground-color is CouleurOption.
      * Ecran d�tailler de la modification du fournisseur avec l'affichage de toutes les information et le menu de modification
       01 ecran-modification-donne-fournisseur.
           10 line 8 col 5 value "Nom du fournisseur      :".
           10 line 8 col 31 using nom-fournisseur foreground-color is CouleurOption.
           10 line 10 col 5 value "Adresse du fournisseur".
           10 line 11 col 5 value "|---- Numero de rue     :".
           10 line 11 col 31 using no-rue foreground-color is CouleurOption.
           10 line 12 col 5 value "|---- Nom de la rue     :".
           10 line 12 col 31 using rue foreground-color is CouleurOption.
           10 line 13 col 5 value "|---- Code Postal       :".
           10 line 13 col 31 using code-postal foreground-color is CouleurOption.
           10 line 14 col 5 value "|---- Ville             :".
           10 line 14 col 31 using ville foreground-color is CouleurOption.
           10 line 15 col 5 value "'---- Pays              :".
           10 line 15 col 31 using pays foreground-color is CouleurOption.
           10 line 17 col  1 pic x(80) value all "_".
           10 line 19 col 10 "1. Modifier. ".
           10 line 20 col 10 "2. Supprimer.".
           10 line 21 col 10 "S. Sauvegarder les changements.".
           10 line 22 col 10 "A. Annuler.".
           10 line 23 col  1 pic x(80) value all "_".
           10 line 24 col 60 value "Choix : ".
           10 line 24 col 69 from option-validation foreground-color is CouleurOption.
      * Ecran de d�part de la modification d'article
       01 ecran-modification-article background-color is CouleurFondEcran foreground-color is CouleurCaractere.
           10 line 6 col 1 pic x(80) value all space background-color is CouleurCaractere.
           10 line 6 col 1 value " Quel article voulez-vous modifiez ?" background-color is CouleurCaractere  foreground-color is CouleurFondEcran.
           10 line 8 col 5 value "Reference         :".
           10 line 8 col 25 from reference-article foreground-color is CouleurOption.
      * Ecran d�tailler de la modification du fournisseur avec l'affichage de toutes les information et le menu de modification
       01 ecran-modification-donne-article.
           10 line  10 col  5 value "Nom de l'Article  :".
           10 line  10 col 25 using nom-article foreground-color is CouleurOption.
           10 line 11 col  5 value "Fournisseur       :".
           10 line 11 col 25 from nom-fournisseur.
           10 line 12 col  5 value "Description       :".
           10 line 12 col 25 using description foreground-color is CouleurOption.
           10 line 13 col  5 value "Quantite en stock :".
           10 line 13 col 25 using quantite  pic 9(10)V,99 foreground-color is CouleurOption .
           10 line 14 col  5 value "Stock minimal     :".
           10 line 14 col 25 using stock-minimum pic 9(10)V,99 foreground-color is CouleurOption.
           10 line 15 col  5 value "Stock maximal     :".
           10 line 15 col 25 using stock-maximum pic 9(10)V,99 foreground-color is CouleurOption.
           10 line 17 col  1 pic x(80) value all "_".
           10 line 19 col 10 value "1. Modifier. ".
           10 line 20 col 10 value "2. Supprimer.".
           10 line 21 col 10 value "S. Sauvegarder les changements.".
           10 line 22 col 10 value "A. Annuler.".
           10 line 23 col  1 pic x(80) value all "_".
           10 line 24 col 60 value "Choix : ".
           10 line 24 col 69 from option-validation foreground-color is CouleurOption.

      * Ecran pour le menu d'affichage
       01 ecran-erreur line 1 col 1 from message-error foreground-color is CouleurCaractere.
      * Ecran de demande pour supprimer un fournisseur (toutes les donn�es)
       01 ecran-suppression.
           05 line 1 col 1 value "Voulez vous tout supprimer(adresse,articles) [O/N] ?".
           05 line 1 col 54 from option-suppression.
      * Ecran de demande de suppression d'un article
       01 ecran-suppression-article.
      * affichagedu cadre
           05 line 7 col 27 pic x(27) value all "_" foreground-color is CouleurDate.
           05 line 8 col 26 value "|" foreground-color is CouleurDate.
           05 line 8 col 54 value "|" foreground-color is CouleurDate.
           05 line 9 col 26 value "|" foreground-color is CouleurDate.
           05 line 9 col 54 value "|" foreground-color is CouleurDate.
           05 line 10 col 26 value "|" foreground-color is CouleurDate.
           05 line 10 col 54 value "|" foreground-color is CouleurDate.
           05 line 11 col 26 value "|" foreground-color is CouleurDate.
           05 line 11 col 54 value "|" foreground-color is CouleurDate.
           05 line 12 col 26 value "|" foreground-color is CouleurDate.
           05 line 12 col 54 value "|" foreground-color is CouleurDate.
           05 line 13 col 26 value "|" foreground-color is CouleurDate.
           05 line 13 col 54 value "|" foreground-color is CouleurDate.
           05 line 14 col 26 value "|" foreground-color is CouleurDate.
           05 line 14 col 54 value "|" foreground-color is CouleurDate.
           05 line 14 col 27 pic x(27) value all "_" foreground-color is CouleurDate.
      * effacement des ligne dans le cadre
           05 line 8 col 27 pic x(26) value all space.
           05 line 9 col 27 pic x(26) value all space.
           05 line 12 col 27 pic x(26) value all space.
           05 line 13 col 27 pic x(26) value all space.
      * Ligne du message
           05 line 10 col 27 value space.
           05 line 10 col 28 value "Voulez-vous supprimer de".
           05 line 11 col 27 value space.
           05 line 11 col 28 value "maniere definitive cet ".
           05 line 12 col 27 value space.
           05 line 12 col 28 value "article  [O/N] ?".
           05 line 13 col 28 using option-suppression.
       procedure division using sqlca DateSysteme.
      *************************************************************************************************************
      * Module de modiffication des articles et des frounisseurs                                                  *
      *************************************************************************************************************
       modification-donnee.
           perform initialisation-modification.
           perform traitement-modification until modification-fin = 1.
           perform fin-modification.
      * Initialisation pour faire les modificaiton
       initialisation-modification.
           move 0 to modification-fin.
           move space to message-error.
      * Traitement des modifications 
       traitement-modification.
           display Modification-des-stocks.
           display ecran-erreur.
           accept option-modification line 6 col 68.
           evaluate true
               when option-modification = "f" or option-modification = "F"
                   perform modification-fournisseur
               when option-modification = "a" or option-modification = "A"
                   perform modification-article
               when option-modification = "r" or option-modification = "R"
                   move 1 to modification-fin
           end-evaluate.
      * fin des modficiaitons
       fin-modification.
           initialize fournisseur.
           initialize article.
           goback.
      **************************************************************************************************************
      * Module de modification d'un fournisseur                                                                    *
      **************************************************************************************************************
       modification-fournisseur.
           perform initialisation-modification-fournisseur.
           perform traitement-modification-fournisseur until fin-donnee = 1.
           perform fin-modification-fournisseur.
      * Initialisation des donn�es pour la modification des fournisseurs
       initialisation-modification-fournisseur.
           initialize message-error.
           initialize fournisseur.
           initialize article.
           display ecran-modification-fournisseur.
           accept nom-fournisseur line 8 col 31.
           if nom-fournisseur = space then 
               move 1 to fin-donnee
               move "Aucun fournisseur avec ce nom existe" to message-error
           else
               perform get-data-fournisseur
           end-if.
      * Traitement de la modification d'un fournisseur
       traitement-modification-fournisseur.
           display ecran-erreur.
           accept option-validation line 24 col 69.
           evaluate true
               when option-validation = "1"
                   accept ecran-modification-donne-fournisseur
               when option-validation = "2"
                   perform suppression-fournisseur
               when option-validation = "s" or option-validation = "S"
                   perform validation-fournisseur
               when option-validation = "a" or option-validation ="A"
                   move space to message-error
                   move 1 to fin-donnee
           end-evaluate.
      * Fin de la modification du fournisseur.
       fin-modification-fournisseur.
           continue.
      * Permet de chercher les donn�es des fournisseurs
       get-data-fournisseur.
           exec sql
             SELECT IdFournisseur, NomFournisseur, idadresse, NoRue,
                    NomRue, CodePostal, Ville, Pays
             into :id-fournisseur, :nom-fournisseur, :id-adresse, :no-rue,
                  :rue, :code-postal, :ville, :pays
             from vue_fournisseur
             where  nomfournisseur = :nom-fournisseur
           end-exec.
           if sqlcode equal 100 then 
               move 1 to fin-donnee
               move "Aucun fournisseur avec ce nom existe" to message-error
      * lancer programme de cr�ation de fournisseur
           else
               move 0 to fin-donnee
               display ecran-modification-donne-fournisseur
           end-if.
      * Validation des nouvelles donn�es pour un fournisseur
       validation-fournisseur.
           exec sql
              select count(*) into :always-exist from fournisseur 
              where idfournisseur <> :id-fournisseur and nomfournisseur = :nom-fournisseur
           end-exec.
           if always-exist > 0 then
               move "Le nom du fournisseur existe deja" to message-error
           else
               perform validation
           end-if.
      * Validation des donn�es (avec tout les tests possibles)
       validation.
           exec sql
             begin tran modication-fournisseur
           end-exec.
           if sqlcode = 0 then
               exec sql
                   UPDATE Fournisseur
                   set NomFournisseur = rtrim(:nom-fournisseur)
                   where idfournisseur = :id-fournisseur
               end-exec
               if sqlcode = 0 then
                   exec sql
                     UPDATE Adresse set
                           NoRue = :no-rue,
                           NomRue = rtrim(:rue),
                           CodePostal = rtrim(:code-postal),
                           Ville = rtrim(:ville),
                           Pays = rtrim(:pays)
                     where idfournisseur = :id-fournisseur and idadresse = :id-adresse  
                   end-exec
                   if sqlcode = 0 then
                       exec sql
                           commit
                       end-exec
                       if sqlcode = 0 then
                           move 1 to fin-donnee
                           move "Enregistrement sauvegarde correctement" to message-error
                       else 
                           move "Erreur sauvegarde" to message-error
                       end-if
                   else
                       move "Erreur modification champs adresse" to message-error
                       exec sql
                         rollback
                       end-exec
                   end-if
               else
                   move "Erreur modification champs nom fournisseur" to message-error
                   exec sql
                       rollback
                   end-exec
               end-if
           else
               move "Erreur transaction" to message-error
           end-if.
      * Demande � l'utilisateur la permission de supprimerr toutes les donn�es concerant le fournisseur
       suppression-fournisseur.
           display ecran-suppression.
           accept option-suppression line 1 col 54.
           if option-suppression = "o" or option-suppression ="O" then
               perform suppression
           end-if.
      * Code pour supprimer
       suppression.
           exec sql 
             begin tran suppression-fournisseur
           end-exec.
           if sqlcode = 0 then
               exec sql 
                 delete from stock where idfournisseur = :id-fournisseur
               end-exec
               if sqlcode = 0 then
                    exec sql
                      delete from article where idfournisseur = :id-fournisseur
                    end-exec
                    if sqlcode = 0 then
                        exec sql 
                            delete from adresse where idfournisseur = :id-fournisseur
                        end-exec
                        if sqlcode = 0 then
                            exec sql
                                delete from fournisseur where idfournisseur = :id-fournisseur
                            end-exec
                            if sqlcode = 0 then
                                exec sql
                                  commit
                                end-exec
                                if sqlcode = 0 then
                                    move "Suppression de toutes les informations du fournisseur" to message-error
                                    move 1 to fin-donnee
                                else 
                                    move "Erreur suppression" to message-error
                                end-if
                            else
                                move "Impossible de supprimer le fournisseur" to message-error
                                exec sql
                                  rollback
                                end-exec
                            end-if
                        else
                            move "Impossible de supprimer les adresses" to message-error
                            exec sql
                              rollback
                            end-exec
                        end-if
                    else
                        move "Impossible de supprimer les articles du fournisseur" to message-error
                        exec sql
                          rollback
                        end-exec
                    end-if
               else
                   move "Erreur suppression stock" to message-error
               end-if

           else
               move "Erreur transaction" to message-error
           end-if.
      ***************************************************************************************************************
      * Module de modification d'un article                                                                          *
      ***************************************************************************************************************
       modification-article.
           perform initialisation-modification-article.
           perform traitement-modification-article until fin-donnee = 1.
           perform fin-modification-article.
      * Initialisation pour le module de modififcation d'un article
       initialisation-modification-article.
           initialize article.
           display ecran-modification-article
           accept reference-article line 8 col 25.
           exec sql
               select idarticle, nomarticle, reference,
                      valeurstock, stockmaxi, stockmini,
                      prix, nomfournisseur, idstock, description
               into :id-article,:nom-article, :reference-article,
                    :quantite, :stock-maximum,:stock-minimum,
                    :prix, :nom-fournisseur, :id-stock,
                    :description
               from vue_article
               where reference = :reference-article
           end-exec.
           if sqlcode = 100 then
               move "Aucun article avec cette reference existe" to message-error
               move 1 to fin-donnee
           else
                   move 0 to fin-donnee
           end-if.
      * Traitement de la modification
       traitement-modification-article.
            display ecran-modification-donne-article.
           display ecran-erreur.
           accept option-validation line 24 col 69.
           evaluate true
               when option-validation = "1"
                   accept ecran-modification-donne-article
               when option-validation = "2"
                   perform supression-article
               when option-validation = "s" or option-validation = "S"
                   perform validation-article
               when option-validation = "a" or option-validation ="A"
                   move space to message-error
                   move 1 to fin-donnee
           end-evaluate.
      * Fin de la modification de l'article
       fin-modification-article.
           continue.
      * Validation de la modification d'un article
       validation-article.
           exec sql
             begin tran validation-article
           end-exec.
           if sqlcode = 0 then
               exec sql
                   update article set
                       nomarticle = rtrim(:nom-article),
                       reference = rtrim(:reference-article),
                       description = rtrim(:description)
                   where idarticle = :id-article
               end-exec
               if sqlcode = 0 then 
      *            divide quantite by 100 giving quantite
                   exec sql 
                       update stock set 
                           valeurstock = :quantite,
                           stockmaxi = :stock-maximum,
                           stockmini = :stock-minimum,
                           prix = :prix
                       where idstock = :id-stock
                   end-exec
                   if sqlcode = 0 then
                       exec sql
                         commit
                       end-exec
                       if sqlcode = 0 then
                           move "Enregisterment sauvegarde correctement" to message-error
                           move 1 to fin-donnee
                       else
                           move "Erreur  sauvegarde" to message-error
                       end-if
                   else 
                       move "Erreur modification stock" to message-error
                   end-if
               else
                   exec sql
                     rollback
                   end-exec
                   move "Erreur modification de l'article" to message-error
               end-if
           else 
               exec sql 
                 rollback
               end-exec
               move "Erreur Transaction" to message-error
           end-if.
      * suppression de l'article avec son stock
       supression-article.
           display ecran-suppression-article.
           accept option-suppression.
           if option-suppression ="o" or option-suppression = "O" then
               perform delete-article
           end-if.
      * Code pour supprimer un article
       delete-article.
           exec sql 
             begin tran suppression-article
           end-exec.
           if sqlcode = 0 then
               exec sql
                 delete from stock where idstock = :id-stock 
               end-exec
               if sqlcode = 0 then 
                   exec sql
                     delete from article where idarticle = :id-article
                   end-exec
                   if sqlcode = 0 then
                       exec sql
                         commit
                       end-exec
                       if sqlcode = 0 then
                           move 1 to fin-donnee
                           move "Suppression de toutes les donn�es de l'article" to message-error
                       else
                           exec sql
                             rollback
                           end-exec
                           move "Erreur de suppression de l'article" to message-error
                       end-if
                   else
                       exec sql 
                         rollback
                       end-exec
                       move "Erreur suppression Article " to message-error
                   end-if
               else
                   exec sql
                     rollback
                   end-exec
                   move "Erreur suppression des stocks de l'article" to message-error
               end-if
           else
               move "Erreur Transaction" to message-error
           end-if.
       end program Modification.