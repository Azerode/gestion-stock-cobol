       program-id. Commande as "FaStock.Commande".
       environment division.
       configuration section.
       special-names.
       currency sign is "�" with picture symbol "$". 
       input-output section.
       file-control.
           select fichier-commande assign to dynamic file-command
               organization line sequential
               access mode sequential.
       data division.
       file section.
           fd fichier-commande.
           01 ligne-commande pic x(88).
       working-storage section.
      ***************************************************************************************************************
      * Structure de donn�e                                                                                         *
      ***************************************************************************************************************
       01 DateSysteme.
           10 Annee pic 99.
           10 Mois pic 99.
           10 Jour pic 99.
       01 date-fichier.
           10 fichier-jour pic 99.
           10 filler pic x value "-".
           10 fichier-mois pic 99.
           10 filler pic x value "-".
           10 fichier-annee pic 99.
      *Entete pour la commande
        01 ligne-vide pic x(85).
      * Titre de la commande
        01 titre.
           05 filler pic x(36).
           05 filler pic x(12) value "Commande n� ".
           05 numero pic x(36).
           05 filler pic x(37).
      * lignes exp�diteur
       01 expediteur-1.
           05 filler pic x(8).
           05 nom-boutique pic x(20) value "FaStock".
           05 filler pic x(56).
       01 expediteur-2.
           05 filler pic x(8).
           05 no-rue-fournisseur pic 999 value 9.
           05 filler pic x.
           05 nom-rue-fournisseur pic x(20) value "rue Icare".
           05 filler pic x(40).
       01 expediteur-3.
           05 filler pic x(8).
           05 code-postal-fa pic x(5) value "67000".
           05 filler pic x.
           05 ville-fa pic x(30) value "Entzheim".
           05 filler pic x(42).
      * lignes fournisseur
       01 fournisseur-1.
           05 filler pic x(54).
           05 nom-fournisseur pic x(20).
           05 filler pic x(8).
       01 fournisseur-2.
           05 filler pic x(54).
           05 no-rue pic 999.
           05 filler pic x.
           05 nom-rue pic x(20).
           05 filler pic x(8).
       01 fournisseur-3.
           05 filler pic x(54).
           05 code-postal pic x(5).
           05 filler pic x.
           05 ville pic x(20).
           05 filler pic x(8).
      * Entete tableau
       01 entete-tableau-ligne.
           05 filler pic x(8).
           05 filler value "+".
           05 filler pic x(22) value all "=".
           05 filler value "+".
           05 filler pic x(21) value all "=".
           05 filler value "+".
           05 filler pic x(14) value all "=".
           05 filler value "+".
           05 filler pic x(13) value all "=".
           05 filler value "+".
       01 entete-tableau-case.
           05 filler pic x(8).
           05 filler pic x value "|".
           05 filler pic x(22) value "Nom article".
           05 filler pic x value "|".
           05 filler pic x(21) value "Reference".
           05 filler pic x value "|".
           05 filler pic x(14) value "Quantite".
           05 filler pic x value "|".
           05 filler pic x(13) value "Prix".
           05 filler pic x value "|".
           05 filler pic x(8).
      * Case tableau
       01 case-tableau.
           05 filler pic x(8).
           05 filler pic x value "|".
           05 nom-article pic x(22).
           05 filler pic x value "|".
           05 reference-article pic x(21).
           05 filler pic x value "|".
           05 quantite pic x(14).
           05 filler pic x value "|".
           05 prix pic $(9)9v,99.
           05 filler pic x value "|".
       01 ligne-tableau.
           05 filler pic x(8).
           05 filler pic x value "+".
           05 filler pic x(22) value all "-".
           05 filler pic x value "+".
           05 filler pic x(21) value all "-".
           05 filler pic x value "+".
           05 filler pic x(14) value all "-".
           05 filler pic x value "+".
           05 filler pic x(13) value all "-".
           05 filler pic x value "+".
           05 filler pic x(8).
      * Ligne total
        01 total.
           05 filler pic x(60).
           05 filler value "Total : ".
           05 prix-total-affichage pic $(9)9v,99. 
      * ligne date
        01 ligne-date.
           05 filler pic x(54).
           05 ligne-jour pic 99.
           05 filler pic x value "/".
           05 ligne-mois pic 99.
           05 filler pic x value "/".
           05 ligne-annee pic 99.
      * Pied de page de la commande
       01 ligne-phrase.
           05 filler pic x(8).
           05 phrase pic x(63) value "Les articles suivant devront �tre livr�s � l'adresse suivante :".
       01 ligne-addresse-1.
           05 filler pic x(12).
           05 livraison-nom pic x(20) value "FaStock Company".
        01 ligne-addresse-2.
           05 filler pic x(12).
           05 no-rue-livraison pic 999 value 9.
           05 filler pic x.
           05 nom-rue-livraison pic x(20) value "rue Icare".
           05 filler pic x(40).
       01 ligne-addresse-3.
           05 filler pic x(12).
           05 code-postal-livraison pic x(5) value "67000".
           05 filler pic x.
           05 ville-livraison pic x(30) value "Entzheim".
           05 filler pic x(42).
      * Fin de la commande
        01 ligne-politesse-1.
           05 filler pic x(61).
           05 politesse-1 pic x(20) value "Cordialement,".
       01 ligne-politesse-2.
           05 filler pic x(61).
           05 politesse-1 pic x(20) value "l'�quipe de FaStock".
      * Valeur pour le stock
       01 stock.
           05 valeurstock pic 9(10)V99.
           05 stock-mini pic 9(10)V99.
           05 stock-maxi pic 9(10)V99.
           05 tmp-stock pic 9(10)V99.
           05 tmp-stock-string sql type char-varying(14).
           05 tmp-string.
               10 tmp-stock-affichage pic z(9)9V,99.
           05 prix-db pic 9(10)V99.
           exec sql
               include sqlca
           end-exec.
      ***************************************************************************************************************
      * Variables                                                                                                   *
      ***************************************************************************************************************
       77 file-command sql type char-varying(200).
       77 path sql type char-varying(100) value "C:\Users\Azero\Documents\Commande\Commande-".
       77 fin-exec pic 9 comp.
       77 ok-generation pic 9 comp.
       77 affichage-total pic 9 comp.
       77 sql-101 pic 9 comp.
       77 commande-fin pic 9 comp.
       77 id-fournisseur pic x(36).
       77 id-commande pic x(36).
       77 comptage pic 9999.
       77 index-fichier pic 9999.
       77 nombre-article pic 9999.
       77 prix-total pic 9(10)V99.
       77 prix-article pic 9(10)V99.
       procedure division.
      ***************************************************************************************************************
      * Module de gn�ration des fichiers de commande                                                                *
      ***************************************************************************************************************
       generation-fichier.
           perform initialisation-generation.
           perform traitement-generation until fin-exec = 1.
           perform fin-generation.
      * Initialisation pour la g�n�ration de commandes
       initialisation-generation.
           move 0 to fin-exec.
           exec sql 
             declare cr-fournisseur cursor for
             select idfournisseur, nomfournisseur,
                    norue, nomrue, codepostal, ville,
                    (select count(*) from vue_article as art where art.idfournisseur = vf.idfournisseur and art.valeurstock <= stockmini ) as nombreArticle
             from vue_fournisseur as vf 
           end-exec.
           if sqlcode = 0 then
               exec sql
                 open cr-fournisseur
               end-exec
               if sqlcode = 0 then
                   move 0 to fin-exec
               else
                   move 1 to fin-exec
               end-if
           end-if.
      * G�n�ration de toutes les commandes
       traitement-generation.
           move 1 to comptage.
           exec sql
             fetch cr-fournisseur into 
               :id-fournisseur, :nom-fournisseur, :no-rue,
               :nom-rue, :code-postal, :ville, :nombre-article
           end-exec.
           if sqlcode not equal 100 and sqlcode not equal 101 then 
               perform commande-article
           else 
               move 1 to fin-exec
           end-if.
      * Fin de la g�n�ration de commande
       fin-generation.
           exec sql
             close cr-fournisseur
           end-exec
           goback.
      ***************************************************************************************************************
      * Module pour �crire les diff�rentes lignes de commande dans le fichier                                       *
      ***************************************************************************************************************
       commande-article.
           perform initialisation-commande.
           perform traitement-commande until commande-fin = 1.
           perform fin-commande.
      * initialisation pour l'�criture des lignes dans le tableau
       initialisation-commande.
           exec sql 
             declare cr-article cursor for 
             select  nomarticle, reference, valeurstock, stockmini,stockmaxi, prix
             from vue_article
             where valeurstock <= stockmini and idfournisseur = :id-fournisseur
           end-exec.
           exec sql
               open cr-article
           end-exec.
           if sqlcode = 0 and nombre-article > 0 then 
               move 0 to commande-fin
           else 
               move 1 to commande-fin
           end-if.
      * Ecriture des lignes dans le tableau
       traitement-commande.
           if nombre-article > 0 then
               perform commande
           end-if.
      * Fin des commandes
       fin-commande.
           exec sql
             close cr-article
           end-exec.
      *************************************************************************************************************
      * MODULE DE GENERARTION DE FICHIER                                                                          *
      *************************************************************************************************************
       commande.
           perform initialisation-file.
           if ok-generation = 0 then 
               perform traitement-file
           end-if.
           perform fin-file.
      * Initialisation des donn�es pour �crire dans le fichier
       initialisation-file.
           initialize file-command.
      * R�cup�ration de la date
           move 0 to sql-101.
           move 0 to affichage-total.
           move 0 to prix-total.
           accept DateSysteme from date.
           move jour to fichier-jour.
           move mois to fichier-mois.
           move Annee to fichier-annee.
      * Cr�ation de la commande dans la base de donn�es
            exec sql 
                select newid() into :id-commande
            end-exec.
            exec sql 
              insert into commande (idcommande) values (:id-commande)
            end-exec.
            if sqlcode = 0 then 
               move 0 to ok-generation
            else
                move 1 to ok-generation
            end-if.
      * Cr�ation du nom de fichier
           string
             path nom-fournisseur "-" date-fichier "_" id-commande ".txt" delimited by "  "
             into file-command
             end-string.
      * Ouverture du fichier
            open output fichier-commande.
      * Remplissage du fichier
       traitement-file.
            perform  entete-fichier.
      * �criture des diff�rents articles � commander pour des fournisseurs
           perform varying index-fichier from 1 by 1 until index-fichier = 11
               exec sql
                 fetch cr-article
                 into :nom-article, :reference-article,
                      :valeurstock, :stock-mini, :stock-maxi, :prix-db
               end-exec
                if sqlcode = 100 or sqlcode = 101 then
                   if affichage-total = 0 then
                       perform fichier-ligne-total
                       move 1 to affichage-total
                   else 
                       perform fichier-ligne-vide
                   end-if
                   move 1 to sql-101
               else
                   perform fichier-ligne-commande
                   exec sql
                       begin tran articlecommande
                   end-exec
                   exec sql
                      insert into articlecommande (
                           id,
                           idCommande,
                           idfournisseur,
                           referenceArticle,
                           quantite,
                           prix
                       ) values (
                           newid(),
                           :id-commande,
                           :id-fournisseur,
                           :reference-article,
                           :tmp-stock,
                           0.00)
                    end-exec
                    if sqlcode = 0 then 
                       exec sql 
                           commit
                       end-exec
                    end-if
               end-if
           end-perform.
           perform pied-page-commande.
      * Fin de fichier de commande
       fin-file.
           close fichier-commande.
           if sql-101 = 1 then
               move 1 to commande-fin
           end-if.
      *************************************************************************************************************
      * DEFINITIONS DES PARTIES DU FICHIER DE COMMANDE                                                            *
      *************************************************************************************************************
      * Entete pour chaque fichier.
       entete-fichier.
           move jour to ligne-jour.
           move mois to ligne-mois.
           move annee to ligne-annee.
           write ligne-commande from ligne-vide.
           write ligne-commande from ligne-vide.
           write ligne-commande from expediteur-1.
           write ligne-commande from expediteur-2.
           write ligne-commande from expediteur-3.
           write ligne-commande from ligne-vide.
           write ligne-commande from ligne-vide.
           write ligne-commande from fournisseur-1.
           write ligne-commande from fournisseur-2.
           write ligne-commande from fournisseur-3.
           write ligne-commande from ligne-vide.
           write ligne-commande from ligne-vide.
           write ligne-commande from ligne-date.
           write ligne-commande from ligne-vide.
           write ligne-commande from ligne-vide.
           move id-commande to numero.
           write ligne-commande from titre.
           write ligne-commande from ligne-vide.
           write ligne-commande from entete-tableau-ligne.
           write ligne-commande from entete-tableau-case.
           write ligne-commande from entete-tableau-ligne.
      * Ecris das lignes vide dans le fichier
       fichier-ligne-vide.
                   write ligne-commande from ligne-vide.
                   write ligne-commande from ligne-vide.
      * �criture du pied de page d'un fichier de commande
       pied-page-commande.
               write ligne-commande from ligne-vide.
               write ligne-commande from ligne-vide.
               write ligne-commande from ligne-phrase.
               write ligne-commande from ligne-addresse-1.
               write ligne-commande from ligne-addresse-2.
               write ligne-commande from ligne-addresse-3.
               write ligne-commande from ligne-vide.
               write ligne-commande from ligne-vide.
               write ligne-commande from ligne-politesse-1.
               write ligne-commande from ligne-politesse-2.
               write ligne-commande from ligne-vide.
               write ligne-commande from ligne-vide.
      * �criture du total de la commande
       fichier-ligne-total.
           move prix-total to prix-total-affichage.
           write ligne-commande from total.
           write ligne-commande from ligne-vide.
      * �criture de la commande dans le fichier 
       fichier-ligne-commande.
      * Calcule de la nouvelle quantit� � commander
                   add valeurstock to stock-mini giving tmp-stock.
                   if tmp-stock > stock-maxi and stock-maxi <> 0 then
                       subtract valeurstock from stock-maxi giving tmp-stock
                   else
                       move stock-mini to tmp-stock
                   end-if
      * Permet de supprimer des espaces devant les nombes
                   move tmp-stock to tmp-stock-affichage.
                   move function reverse(tmp-string) to tmp-string.
                   move tmp-string to tmp-stock-string.
                   unstring 
                     tmp-stock-string delimited by space
                     into 
                       quantite
                   end-unstring.
                   move function reverse(quantite) to quantite.
                   multiply tmp-stock by prix-db giving prix-article.
                   move prix-article to prix.
                   add prix-article to prix-total.
                   write ligne-commande from case-tableau.
                   write ligne-commande from ligne-tableau.
       end program Commande.