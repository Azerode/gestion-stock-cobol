       program-id. Ajout as "FaStock.Ajout".

       data division.
       working-storage section.
       01 fournisseur.
           05 id-fournisseur       pic x(36).
           05 nom-fournisseur pic x(50).
           05 id-adresse          pic x(36).
           05 adresse-fournisseur.
               10 No-Rue            pic x(3).
               10 nom-rue           pic x(20).
               10 code-postal       pic 9(5).
               10 ville            pic x(15).
               10 pays             pic x(15).
               10 adresse          pic x(130).
      * D�finition des donn�es pour un article
       01 article.
           05 id-article pic x(36).
           05 nom-article pic x(50).
           05 reference-article pic x(50).     
           05 description pic x(50).
           05 prix pic 9(10)V99.
           05 quantite pic 9(10)v99.
           05 stock-minimum pic 9(10)v99.
           05 stock-maximum pic 9(10)v99.
       01 stock.
           10 id-stock     pic x(36).
           
      * Tableau pour stocker une page.    
       01 tableau-article.
           05 taille pic 99 comp value 0.
           05 case occurs 10.
               10 case-id pic x(36).
               10 case-nom pic x(50).
               10 case-reference pic x(50).
               10 case-prix pic z(10)9V,99.
               10 case-quantite pic z(10)v99.
               10 case-stock-minimum pic z(10)9v,99.
               10 case-stock-maximum pic z(10)9v,99.

       77 CouleurFondEcran         pic 99 value 0.
       77 CouleurCaractere         pic 99 value 2.
       77 CouleurDate              pic 99 value 6.
       77 CouleurOption            pic 99 value 15.
      ****Variable
       77 OptionAjout              pic x.
       77 ValidationAjout   pic x.
       77 exist                    pic 999.
       77 message-error            pic x(80).
       77 validation               pic x(80).
       77 Confirmation             pic x.
      **** variable pour le tableau et l'affichage du tableau
       77 Incrementation           pic 99.
       77 comptage pic 99 comp.
       77 Ligne-Affichage              pic 99.


       linkage section.
           exec sql
               include sqlca
           end-exec.
       01 DateSysteme.
           10 Annee pic 99.
           10 Mois pic 99.
           10 Jour pic 99.
       screen section.
      **** Ecran de base (apres le choix dans le menu)
       01 Ecran-Ajout background-color is CouleurFondEcran foreground-color is CouleurCaractere.
           10 line 24 col  1 blank screen.
           10 line 24 col 2 from Jour of DateSysteme foreground-color is CouleurDate.
           10 line 24 col 4 value "/" foreground-color is CouleurDate.
           10 line 24 col 5 from Mois of DateSysteme foreground-color is CouleurDate.
           10 line 24 col 7 value "/" foreground-color is CouleurDate.
           10 line 24 col 8 from Annee of DateSysteme foreground-color is CouleurDate.
           10 line  3 col 38 value "Ajout".
           10 line  5 col  5 value "Nom du fournisseur     :".
           10 line  5 col 42 from nom-fournisseur foreground-color is CouleurOption.
           10 line  6 col  5 value "Adresse du founisseur  :".
           10 line  7 col  1 pic x(80) value all "_".
           10 line 20 col  1 pic x(80) value all "_".
      **** Ecan principale de l'ajout d'un article
       01 Ecran-Ajout-Article background-color is CouleurFondEcran foreground-color is CouleurCaractere.
           10 line 24 col  1 blank screen.
           10 line 24 col 2 from Jour of DateSysteme foreground-color is CouleurDate.
           10 line 24 col 4 value "/" foreground-color is CouleurDate.
           10 line 24 col 5 from Mois of DateSysteme foreground-color is CouleurDate.
           10 line 24 col 7 value "/" foreground-color is CouleurDate.
           10 line 24 col 8 from Annee of DateSysteme foreground-color is CouleurDate.
           10 line  3 col 38 value "Ajout".
           10 line  5 col  5 value "Nom du fournisseur     :".
           10 line  5 col 30 from nom-fournisseur foreground-color is CouleurOption.
           10 line  6 col  5 value "Adresse du founisseur  :".
           10 line  7 col  1 pic x(80) value all "_".
           10 line 20 col  1 pic x(80) value all "_".
           10 line  6 col 30 from adresse.
           10 line 22 col 10 value "[A]jouter un article, [R]etour :".
           10 line 22 col 43 from OptionAjout foreground-color is CouleurOption.
      **** Ecran qui affiche les article entree sur l'ecran au dessu
       01 ecran-affichage.
           05 line ligne-affichage col 1 pic 99 from Incrementation.
           05 line ligne-affichage col 4 pic x(20) from case-nom(Incrementation).
           05 line ligne-affichage col 25 pic x(20) from case-reference(Incrementation).

      **** Ecran qui demande si l'utilisateur veut creer le fournisseur
       01 q-ajout-Fournisseur.
           10 line 22 col 10 "Client inconnu, voulez vous le creer ? (O/N)".
           10 line 22 col 55 from OptionAjout foreground-color is CouleurOption.
      **** Ecran qui permet d'entrer les information pour creer un article
       01 m-ajout-article.
           10 line  1 col  1 Blank Screen.
           10 LINE  8 col  1 pic x(80) value all space background-color is CouleurCaractere foreground-color is CouleurFondEcran.
           10 line  8 col 20 value "Ajout d'un article pour le fournisseur" background-color is CouleurCaractere foreground-color is CouleurFondEcran.
           10 line  8 col 59 from nom-fournisseur pic x(20) background-color is CouleurCaractere foreground-color is CouleurFondEcran.
           10 line 10 col  5 value "Nom de l'Article  :".
           10 line 10 col 25 using nom-article foreground-color is CouleurOption.
           10 line 11 col  5 value "Reference-article :".
           10 line 11 col 25 using reference-article foreground-color is CouleurOption.
           10 line 12 col  5 value "Description       :".
           10 line 12 col 25 using Description foreground-color is CouleurOption.
           10 line 13 col  5 value "Prix              :".
           10 line 13 col 25 using prix pic z(16)9,99 foreground-color is CouleurOption.
           10 line 14 col  5 value "Quantite en stock :".
           10 line 14 col 25 using quantite pic z(16)9,99 foreground-color is CouleurOption.
           10 line 15 col  5 value "Stock minimal     :".
           10 line 15 col 25 using stock-minimum pic z(16)9,99 foreground-color is CouleurOption.
           10 line 16 col  5 value "Stock maximal     :".
           10 line 16 col 25 using stock-maximum pic z(16)9,99 foreground-color is CouleurOption.
           10 line 21 col  1 pic x(80) value all "_".
      **** Ecran qui permet d'entrer les information pour creer un article
       01 m-ajout-fournisseur.
           10 LINE  5 col  1 pic x(80) value all space.
           10 LINE  6 col  1 pic x(80) value all space.
           10 line  8 col  5 value "Nom du fournisseur     :".
           10 line  8 col 30 using nom-fournisseur.
           10 line 10 col 5 value "Adresse du fournisseur".
           10 line 11 col 5 value "|---- Numero de rue     :".
           10 line 11 col 31 using no-rue foreground-color is CouleurOption.
           10 line 12 col 5 value "|---- Nom de la rue     :".
           10 line 12 col 31 using nom-rue foreground-color is CouleurOption.
           10 line 13 col 5 value "|---- Code Postal       :".
           10 line 13 col 31 using code-postal foreground-color is CouleurOption.
           10 line 14 col 5 value "|---- Ville             :".
           10 line 14 col 31 using ville foreground-color is CouleurOption.
           10 line 15 col 5 value "'---- Pays              :".
           10 line 15 col 31 using pays foreground-color is CouleurOption.
      **** Ecran qui genere le message d'erreur
       01 ecran-erreur line 1 col 1 from message-error foreground-color is CouleurCaractere.
      **** Ecran qui permet de valider les information entrer pour cr�er un article
       01 Ecran-Validation.
           10 LINE 22 col  1 pic x(80) value all space.
           10 line 22 col 10 "c. Confirmer.".
           10 line 22 col 45 "A. Annuler.".
           10 line 24 col 60 value "Choix : ".
           10 line 24 col 69 from ValidationAjout foreground-color is CouleurOption.
      **** ecran qui affiche que le fournisseur a �t� cr�er avec succ�s
       01 Ecran-creation.
           10 line 22 col 10 from validation.

       procedure division using sqlca DateSysteme.

       
           perform Ajout-init.
           perform Ajout-trt until OptionAjout ="r" or OptionAjout = "R".
           perform Ajout-fin.

       Ajout-init.
           Move "" to OptionAjout.
       AJout-trt.
           initialize fournisseur.
           display Ecran-Ajout.
           Accept nom-fournisseur line 5 col 30.
           if nom-fournisseur = "" then
               move "R" to optionajout
           else
           exec sql
      **** Check si le fournisseur ecrit existe dans la bdd

             select count(*) into :exist
             from fournisseur
             where NomFournisseur = :nom-fournisseur
           end-exec
           if exist = 0 then
      *** si il existe pas on lui propose de le cr�er
               move "N" to OptionAjout
               display q-ajout-Fournisseur
               accept OptionAjout line 22 col 55
               if OptionAjout = "O" or OptionAjout = "o" then
                   perform ajout-Fournisseur
               end-if
           else
      **** si il existe il est envoy� dans la section cr�ation d'un article
               perform Ajout-question
           end-if
           end-if.
           continue.

        

       Ajout-fin.
           goback.

       Ajout-question.
           perform  Ajout-question-init
           perform Ajout-question-trt until OptionAjout = "r" or optionajout ="R".


       Ajout-question-init.
      **** on r�cup�re les information li�e au fournisseur
           exec sql
             select  IdFournisseur into :id-fournisseur
             from Vue_Fournisseur
             where NomFournisseur = :nom-fournisseur
           end-exec.
           exec sql
             select adresse into :adresse
             from adresse
             where IdFournisseur = :id-fournisseur
           end-exec.
      **** test si la connection a la bdd a bien march�
           if sqlcode not eqUal 0 then
             Move "erreur chargement adresse fournisseur" to message-error
             display ecran-erreur
           end-if.
      **** param�trage des variables utile au tableau
           move 1 to taille.
           move 1 to comptage.


       Ajout-question-trt.
           Move 8 to Ligne-Affichage.             
           move "R" to OptionAjout.
      **** on affiche l'Ecan principale de l'ajout d'un article
           display Ecran-Ajout-Article.
      **** on lancela partie qui affiche l'ecran qui envoie les donn�e dans le tableau (la premi�re fois le tableau �tant vide rien ne s'affiche
           perform Affichage-ajout varying Incrementation from 1 by 1 until Incrementation = taille.
           accept OptionAjout line 22 col 43.
      **** On dirige l'utilisateur en fonction de sont choix
           evaluate true
      **** Ajouter un nouvel article
               when OptionAjout = "a" or OptionAjout ="A"
                   perform Ajout-Article
      **** Retour au menu
               when OptionAjout = "r" or OptionAjout ="R"
                   continue
           end-evaluate.


       Affichage-ajout.
      **** affiche l'ecran qui envoie les donn�e stock� dans le tableau
           display ecran-affichage.
           add 1 to Ligne-Affichage.
       Ajout-Article.
           perform Ajout-Article-init.
           perform Ajout-Article-trt until ValidationAjout ="a" or ValidationAjout ="A" or ValidationAjout="c" or ValidationAjout= "C".

       Ajout-article-init.
      **** Initialisation de la structure de donn�e pour que les variable n'ai plus les donn�e d'un pr�c�dent article
           initialize article.
           move "" to ValidationAjout.
       Ajout-Article-trt.
      **** Affichage de l'Ecran qui permet d'entrer les information pour creer un article
           display m-ajout-article.
           accept m-ajout-article.
           if nom-article not equal "" then
               if reference-article not equal "" then
                   exec sql
      **** Check si le fournisseur ecrit existe dans la bdd

                      select count(*) into :exist
                      from article
                      where reference = :reference-article
                   end-exec
                   if exist = 0 then

                   
                   if description not equal "" then
                       if quantite not equal 0 then
                           if stock-minimum not equal 0 then
                               if stock-maximum not equal "" then
                                   if prix not equal 0 then
      **** Affichage de l'ecran validation
                                       display Ecran-Validation
                                       accept ValidationAjout line 24 col 69
      **** On prends des new id pour le nuvel article
                                       exec sql
                                           select newid() into :id-article
                                       end-exec
                                       exec sql
                                           select newid() into :id-stock
                                       end-exec
                                       divide 100 into prix
                                       divide 100 into quantite
                                       divide 100 into stock-minimum
                                       divide 100 into stock-maximum

      **** si l'utilisateur confirme sont choix on insert dans la bdd les donn�es.
                                       if ValidationAjout = "c" or ValidationAjout = "C" then
                                       exec sql
                                           insert into Article
                                           (IdArticle, IdFournisseur, NomArticle, Reference, Description)
                                           values (:id-article, :id-fournisseur, rtrim(:nom-article), rtrim(:reference-article), :description) 
                                       end-exec
                                           if sqlcode = 0 then
                                           exec sql
                                               insert into stock
                                               (IdArticle, IdFournisseur, IdStock, ValeurStock, StockMini, StockMaxi, prix)
                                               values (:id-article, :id-fournisseur, :id-stock, :quantite, :stock-minimum, :stock-maximum, :prix) 
                                           end-exec
                                               if sqlcode = 0 then
                                               exec sql                               
                                                   commit
                                               end-exec
                                                   if sqlcode = 0 then
      **** on sauvegarde les information dans un tableau 
                                                       move "Fournisseur cree" to validation
                                                       display Ecran-creation
                                                       move nom-article to case-nom(comptage)
                                                       move quantite to case-quantite(comptage)
                                                       move reference-article to case-reference(comptage)
                                                       move stock-minimum to case-stock-minimum(comptage)
                                                       move stock-maximum to case-stock-maximum(comptage)
                                                       add 1 to taille
                                                       add 1 to comptage
                                                   end-if
                                               end-if
                                           else
                                               move "Erreur SQL creation Fournisseur" to message-error
                                               display ecran-erreur
                                           end-if
                                       end-if
                                   else
                                       display "erreur champ prix vide" line 1 col 2
                                       accept Confirmation
                                   end-if
                               else
                                   display "erreur champ vide" line 1 col 2
                                   accept Confirmation
                               end-if
                           else
                               display "erreur champ vide" line 1 col 2
                               accept Confirmation
                           end-if
                       else
                           display "erreur champ quantite vide" line 1 col 2
                           accept Confirmation
                       end-if
                   else
                       display "erreur champ description vide" line 1 col 2
                       accept Confirmation
                   end-if
                   else 
                       display "erreur reference dej� existante" line 1 col 2
                       accept Confirmation
                   end-if
               else
                   display "erreur champ reference vide" line 1 col 2
                   accept Confirmation
               end-if
           else
               display "erreur champ nom de l'article  vide" line 1 col 2
               accept Confirmation
           end-if.

       Ajout-Fournisseur.
           perform Ajout-Fournisseur-init.
           perform Ajout-Fournisseur-trt until ValidationAjout ="a" or ValidationAjout ="A" or ValidationAjout="c" or ValidationAjout= "C".

       Ajout-Fournisseur-init.
           move "" to ValidationAjout.

       Ajout-Fournisseur-trt.
           display m-ajout-fournisseur.
           accept m-ajout-fournisseur.
           if no-rue not equal 0 then
               if nom-rue not equal "" then
                   if code-postal not equal 0 then
                       if ville not equal "" then
                           if pays not equal "" then
      **** Affichage de l'ecran validation
                           display Ecran-Validation
                           accept ValidationAjout line 24 col 69
      **** On prends des new id pour le nuvel article
                           exec sql
                               select newid() into :id-fournisseur
                           end-exec
                           exec sql
                               select newid() into :id-adresse
                           end-exec
      ****on insert dans la bdd les donn�es.
                           if ValidationAjout = "c" or ValidationAjout = "C" then

                           exec sql
                               insert into Fournisseur
                               (IdFournisseur, Nomfournisseur)
                               values (:id-fournisseur, :nom-fournisseur)
                           end-exec
                           if SQLCODE equal 0 then
                           exec sql
                               insert into Adresse
                               (IdAdresse,IdFournisseur, NoRue,NomRue, CodePostal,Ville,Pays)
                               values (:id-adresse, :id-fournisseur, :no-rue, rtrim(:nom-rue), :code-postal, rtrim(:ville), rtrim(:pays) )
                           end-exec
                               if SQLCODE equal 0 then
                               exec sql
                                   commit
                               end-exec
                                   if SQLCODE equal 0 then
                                       move "Fournisseur cree" to validation
                                       display Ecran-creation
                                   end-if
                               end-if
                           else
                               move "Erreur SQL creation Fournisseur" to message-error
                               display ecran-erreur
                           end-if
                           end-if
                           else
                               display "erreur champ pays vide" line 1 col 2

                           end-if
                       else
                           display "erreur champ ville vide" line 1 col 2
                           accept Confirmation
                       end-if
                   else
                       display "erreur champ code postal vide" line 1 col 2
                       accept Confirmation
                   end-if
               else
                   display "erreur champ nom de la rue vide" line 1 col 2
                   accept Confirmation
               end-if
           else
               display "erreur champ numero de la rue vide" line 1 col 2
               accept Confirmation
           end-if.
       end program Ajout.