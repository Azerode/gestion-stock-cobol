       program-id. stockvalorise as "FaStock.stockvalorise".
       environment division.
       input-output section.
       file-control.
           select Stock-valoriser assign to dynamic CheminFichier
           organization is line sequential.



       data division.
       fd Stock-valoriser record varying from 0 to 255.
       01 enregistrementfichier pic x(255).
       working-storage section.
       01 article.
           05 id-article pic x(36).
           05 nom-article pic x(50).
           05 reference-article pic x(50).     
           05 description pic x(50).
           05 prix pic 9(10)V99.
           05 quantite pic 9(10)v99.
           05 stock-minimum pic 9(10)v99.
           05 stock-maximum pic 9(10)v99.
       01 fournisseur.
           05 nom-fournisseur pic x(50).
           05 adresse-fournisseur pic x(130).

       01 DateSysteme.
           10 Annee pic 99.
           10 Mois pic 99.
           10 Jour pic 99.

       01 calcul.
           10 Total-Article        pic 9(18)v99.
           10 Total-stock          pic 9(18)v99.
           10 Total-fournisseur    pic 9(18)v99.

       01 Stock-Entete1.
           10 FILLER PIC X(17) value all space.
           10 FILLER PIC X(20) Value "Stock Valoris�".

       01 Stock-Entete2.
           10 FILLER PIC X(7) Value "Date : ".
           10 Jour PIC Z9.
           10 FILLER PIC X value "/".
           10 Mois PIC 99.
           10 FILLER PIC X value "/".
           10 Annee PIC 99.
 
       01  Stock-Entete3.
           10 FILLER PIC X(80) value all "=".

       01 Stock-fournisseur.
           10 filler pic x(21) value "Nom du fournisseur : ".
           10 Nom-fournisseur pic x(50).

       01 stock-Article.
           10 filler pic x(3) value " - ".
           10 Nom-article pic x(15).
           10 filler pic x(5) value all space.
           10 total-article pic z(18)v,99.
           10 filler pic x(1) value "�".

       01 stock-fournisseur-fin.
           10 filler pic x(30) value all space.
           10 filler pic x(8) value "total : ".
           10 total-fournisseur pic z(18)v,99.
           10 filler pic x(1) value "�".

       01 separation.
           10 filler pic x(80) value all "=".

       01 stock-total.
           10 filler pic x(23) value all space.
           10 filler pic x(15) value "stock total : ".
           10 Total-stock pic z(18)v,99.
           10 filler pic x(1) value "�".
       01 sautligne.
           10 filler pic x(80 ) value all space.




           exec sql
               include sqlca
           end-exec.
       77 cheminfichier pic x(255) value "C:\Users\Azero\Documents\Fastock\Stock-valoriser.txt".
       77 connexion-database pic x(255) value "Trusted_Connection=yes;Database=FaStock;server=localhost\SQLEXPRESS;factory=System.Data.SqlClient;".
       77 fin-trt              pic 9.
       77 reponse              pic x.
       77 fin-calcul           pic x.

       procedure division.
       main.
           perform main-init.
           perform main-trt until fin-trt = 1.
           perform main-fin.
       
       main-init.
           open output stock-valoriser
           exec sql
             connect using :connexion-database
           end-exec.
           accept DateSysteme from Date.
           move 0 to fin-trt.
           exec sql
               Declare Curseur-fournisseur cursor for
                   select NomFournisseur from Fournisseur
           end-exec.
           move 0 to Total-stock of calcul .
           exec sql
               open Curseur-fournisseur
           end-exec.
           move corresponding DateSysteme to Stock-Entete2.
           perform stockvalorise_ImpressionEntete.
      *    string cheminfichier,DateSysteme,".txt" delimited by "," into cheminfichier.

       main-trt.
           write enregistrementfichier from sautligne.
           write enregistrementfichier from separation.
           write enregistrementfichier from sautligne.
           exec sql
               fetch Curseur-fournisseur into :fournisseur.nom-fournisseur
           end-exec.

            if SQLCODE = 100 or SQLCODE = 101 then
                move 1 to fin-trt
            else
                if SQLCODE not equal 0
                    display " erreur bdd"
                    accept reponse
                else
                    move corresponding fournisseur to Stock-fournisseur                          
                    write enregistrementfichier from Stock-fournisseur
                    write enregistrementfichier from sautligne
                    perform calcul-stock
                    move corresponding calcul to stock-fournisseur-fin
                    write enregistrementfichier from stock-fournisseur-fin
                end-if
            end-if.
           

       main-fin.
           exec sql
               close Curseur-fournisseur
           end-exec.
           move corresponding calcul to stock-total.
           write enregistrementfichier from separation.
           write enregistrementfichier from sautligne.
           write enregistrementfichier from stock-total.
           stop run.
           
       calcul-stock.
           perform calcul-stock-init.
           perform calcul-stock-trt until fin-calcul = 1.
           perform calcul-stock-fin.

       calcul-stock-init.
           move 0 to fin-calcul.
           move 0 to Total-fournisseur of calcul.
           exec sql
               Declare Curseur-stock cursor for
                   select NomArticle, ValeurStock, prix from Vue_article
                   where NomFournisseur = :Fournisseur.Nom-fournisseur
           end-exec.
           exec sql
               open Curseur-stock
           end-exec.

       calcul-stock-trt.
           move 0 to Total-Article of calcul.

           exec sql
               fetch Curseur-stock into :article.Nom-article, :Quantite, :prix
           end-exec.
            if SQLCODE = 100 or SQLCODE = 101 then
                move 1 to fin-calcul
                add Total-fournisseur of calcul to Total-stock of calcul
            else
                if SQLCODE not equal 0
                    display " erreur bdd"
                    accept reponse
                else
                    multiply quantite by prix giving Total-Article of calcul
                    add Total-Article of calcul to Total-fournisseur of calcul
                    move corresponding article to stock-Article
                    move corresponding calcul to stock-Article
                    write enregistrementfichier from stock-Article
                end-if
            end-if.



       calcul-stock-fin.
           exec sql
               close Curseur-stock
           end-exec.


       stockvalorise_ImpressionEntete.
           write enregistrementfichier from Stock-Entete1.
           write enregistrementfichier from Stock-Entete2.
           write enregistrementfichier from Stock-Entete3.


       end program stockvalorise.